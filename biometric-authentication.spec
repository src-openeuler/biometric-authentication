Name:           biometric-authentication
Version:        0.9.72
Release:        10
Summary:        biometric-authentication
License:        LGPL-3.0
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz
Patch01:        0001-add-getenv.patch
Patch02:        0002-delete-biometric-authenticationd-SingleInstance.patch

%ifarch x86_64
%global platform x86_64
%endif
%ifarch aarch64
%global platform aarch64
%endif
%ifarch riscv64
%global platform riscv64
%endif
%ifarch loongarch64
%global platform loongarch64
%endif

BuildRequires: python3
BuildRequires: automake
BuildRequires: intltool
BuildRequires: gettext-devel
BuildRequires: libtool
BuildRequires: glib2-devel
BuildRequires: gtk3-devel
BuildRequires: libusb-devel
BuildRequires: sqlite-devel
BuildRequires: libfprint-devel
BuildRequires: polkit-devel
BuildRequires: uuid-devel
BuildRequires: chrpath

Requires: systemd
Requires: python-prettytable
Requires: dbus-python


%description
biometric identification framework

%package -n biometric-auth
Summary:  libs
License:  LGPL-3.0 
Provides: biometric-auth
Requires: libbiometric0 systemd
 
%description -n biometric-auth
Biometric Authentication Service


%package -n biometric-driver-community-multidevice
Summary:  libs
License:  LGPL-3.0 
Provides: biometric-driver-community-multidevice
Requires: libbiometric0 biometric-utils

%description -n biometric-driver-community-multidevice
ometric Authentication Driver (community multidevice)

%package -n biometric-utils
Summary:  biometric-utils
License:  LGPL-3.0
Provides: biometric-utils
Requires: biometric-auth systemd python3-prettytable python3-dbus python3-gobject

%description -n biometric-utils
Biometric authentication utils



%package -n libbiometric-devel
Summary:  libbiometric-devel
License:  LGPL-3.0 
Requires: libbiometric0 systemd

%description -n libbiometric-devel
Biometric Identification DRIVER API - development files



%package -n libbiometric0
Summary:  libbiometric0
Requires: systemd
License:  LGPL-3.0

%description -n libbiometric0
Biometric Identification library

%prep
%autosetup -n %{name}-%{version} -p1

%build
export prefix=/usr
./autogen.sh
./configure --build=%{platform}-linux-gnu \
    --prefix=/usr \
    --includedir=\${prefix}/include \
    --mandir=\${prefix}/share/man \
    --infodir=\${prefix}/share/info \
    --sysconfdir=/etc \
    --localstatedir=/var \
    --disable-silent-rules \
    --libdir=\${prefix}/lib/%{platform}-linux-gnu \
    --libexecdir=\${prefix}/lib/%{platform}-linux-gnu \
    --disable-dependency-tracking \
    --enable-static \
    --enable-shared \
    --with-bio-db-dir=/var/lib/biometric-auth/ \
    --with-bio-db-name=biometric.db \
    --with-bio-config-dir=/etc/biometric-auth/ \
    --with-bio-driver-dir=/usr/lib/biometric-authentication/drivers \
    --with-bio-extra-dir=/usr/lib/biometric-authentication/drivers/extra \
    --libexecdir=\${prefix}/lib/biometric-authentication

%{make_build}

%install
rm -rf $RPM_BUILD_ROOT 
make install DESTDIR=%{buildroot}

chrpath -d %{buildroot}/usr/lib/biometric-authentication/biometric-authenticationd
chrpath -d %{buildroot}/usr/lib/biometric-authentication/drivers/*.so

touch %{name}-%{platform}.conf
echo "%{_prefix}/lib/%{platform}-linux-gnu"  >> %{name}-%{platform}.conf
mkdir -p $RPM_BUILD_ROOT/etc/ld.so.conf.d
install -p -m644 %{name}-%{platform}.conf $RPM_BUILD_ROOT/etc/ld.so.conf.d/

%clean
rm -rf $RPM_BUILD_ROOT

%posttrans
%ldconfig_scriptlets

%files
%files -n biometric-auth
%{_sysconfdir}/dbus-1/system.d/org.ukui.Biometric.conf
%{_sysconfdir}/init.d/biometric-authentication
/usr/lib/biometric-authentication/biometric-authenticationd
%{_datadir}/dbus-1/interfaces/org.ukui.Biometric.xml
%{_datadir}/polkit-1/actions/org.freedesktop.policykit.pkexec.biometric-authentication.policy
%{_datadir}/polkit-1/actions/org.ukui.biometric.policy
%{_sysconfdir}/biometric-auth/biometric-drivers.conf
/lib/systemd/system/biometric-authentication.service


%files -n biometric-driver-community-multidevice
/usr/lib/biometric-authentication/discover-tools/extra/community-multidevice-discover-tool
/usr/lib/biometric-authentication/drivers/*
/usr/lib/biometric-authentication/discover-tools/usb*



%files -n biometric-utils
%{_bindir}/biometric-auth-client
%{_bindir}/biometric-config-tool
%{_bindir}/biometric-device-discover



%files -n libbiometric-devel
%{_includedir}/libbiometric/biometric_common.h
%{_includedir}/libbiometric/biometric_config.h
%{_includedir}/libbiometric/biometric_storage.h
%{_includedir}/libbiometric/biometric_version.h


%files -n libbiometric0
%{_datadir}/locale/bo/LC_MESSAGES/biometric-authentication.mo
%{_datadir}/locale/es/LC_MESSAGES/biometric-authentication.mo
%{_datadir}/locale/fr/LC_MESSAGES/biometric-authentication.mo
%{_datadir}/locale/pt/LC_MESSAGES/biometric-authentication.mo
%{_datadir}/locale/ru/LC_MESSAGES/biometric-authentication.mo
%{_datadir}/locale/zh_CN/LC_MESSAGES/biometric-authentication.mo
%{_prefix}/lib/%{platform}-linux-gnu/libbiometric.*
%{_prefix}/lib/%{platform}-linux-gnu/pkgconfig/libbiometric.pc
%{_sysconfdir}/ld.so.conf.d/%{name}-%{platform}.conf


%changelog
* Tue Dec 10 2024 peijiankang <peijiankang@kylinos.cn> - 0.9.72-10
- fix license info

* Tue Jun 18 2024 Wenlong Zhang <zhangwenlong@loongson.cn> - 0.9.72-9
- fix build build error for loongarch64

* Tue Mar 14 2023 peijiankang <peijiankang@kylinos.cn> - 0.9.72-8
- delete biometric-authenticationd SingleInstance

* Tue Mar 07 2023 peijiankang <peijiankang@kylinos.cn> - 0.9.72-7
- fix biometric-authentication service start error 

* Wed Mar 01 2023 misaka00251 <liuxin@iscas.ac.cn> - 0.9.72-6
- Fix riscv64 build error

* Wed Mar 01 2023 peijiankang <peijiankang@kylinos.cn> - 0.9.72-5
- remove rpath of biometric-authentication

* Tue Feb 07 2023 tanyulong <tanyulong@kylinos.cn> - 0.9.72-4
- Enable debuginfo for fix strip

* Wed Aug 3 2022 peijiankang <peijiankang@kylinos.cn> - 0.9.72-3
- remove if and endif

* Fri Jun 24 2022 peijiankang <peijiankang@kylinos.cn> - 0.9.72-2
- fix build error

* Mon May 16 2022 tanyulong <tanyulong@kylinos.cn> - 0.9.72-1
- init package for openEuler
